import pydash as _
from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField

from .models import Pokemon, Evolution, BaseStat

default_exclude = [
    'created',
    'modified',
]


class BaseStatSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseStat
        exclude = _.union(default_exclude, [])


class PokemonEvolSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pokemon
        fields = (
            Pokemon._meta.pk.name,
            'name',
        )


class EvolutionSerializer(serializers.ModelSerializer):
    evolves_to = RecursiveField(required=False, allow_null=True)
    pokemon = RecursiveField(PokemonEvolSerializer.__name__)

    class Meta:
        model = Evolution
        exclude = _.union(default_exclude, [])


class PokemonSerializer(serializers.ModelSerializer):
    base_stats = BaseStatSerializer(many=True)
    evolutions = EvolutionSerializer(many=True)

    class Meta:
        model = Pokemon
        fields = tuple(_.union([
            field.name for field in Pokemon._meta.fields
            if not _.includes(_.union(default_exclude, []), field.name)
        ], [
            'base_stats',
            'evolutions',
        ]))
