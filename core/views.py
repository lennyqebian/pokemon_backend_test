from django.db.models import QuerySet
from rest_framework.generics import ListAPIView

from core.models import Pokemon
from core.serializers import PokemonSerializer


class PokemonSearchApiView(ListAPIView):
    serializer_class = PokemonSerializer

    def get_queryset(self):
        name = self.kwargs['name']
        queryset: QuerySet = Pokemon.objects.filter(name__icontains=name)
        return queryset
