from pprint import pformat
from typing import Dict, Any, List

import pydash as _
import requests
from django.core.management.base import BaseCommand, CommandParser
from django.conf import settings
from django.db.models import QuerySet
# noinspection PyProtectedMember
from flatten_dict import flatten
from requests import Response

from core.models import Pokemon, BaseStat, Evolution


class Command(BaseCommand):
    help = 'Fetches and stores a Pokémon evolution chain from PokéApi ' \
           '(https://pokeapi.co/docs/v2)'

    def add_arguments(self, parser: CommandParser):
        parser.add_argument('evolution_chain_id',
                            type=str,
                            help='ID representing the Evolution Chain')

    def handle(self, *args, **kwargs):
        evolution_chain = self._fetch_evolution_chain(**kwargs)

        species = self._fetch_species(evolution_chain['chain'])

        pokemon = self._fetch_pokemon(species)

        pokemon_model = self._store_pokemon(pokemon)

        self._store_base_stats(pokemon['stats'], pokemon_model)

        self._store_evolutions(pokemon_model, evolution_chain['chain'])

        self.stdout.write(f'** Evolution Chain saved **: \n{pformat(evolution_chain)}')
        self.stdout.write(f'-> Evolution Chain saved successfully! <-')

    @staticmethod
    def _fetch_evolution_chain(**kwargs) -> Dict[str, Any]:
        evolution_chain_id: str = kwargs['evolution_chain_id']
        evolution_chain_url = f'{settings.POKE_API_BASE_URL}/evolution-chain/{evolution_chain_id}'
        evolution_chain_res: Response = requests.get(evolution_chain_url)
        evolution_chain: Dict[str, Any] = evolution_chain_res.json()
        return evolution_chain

    @staticmethod
    def _fetch_species(evol_chain: Dict[str, Any]) -> Dict[str, Any]:
        species_url: str = evol_chain['species']['url']
        species_res: Response = requests.get(species_url)
        species: Dict[str, Any] = species_res.json()
        return species

    @staticmethod
    def _fetch_pokemon(species: Dict[str, Any]) -> Dict[str, Any]:
        pokemon_id = species['id']
        pokemon_url = f'{settings.POKE_API_BASE_URL}/pokemon/{pokemon_id}'
        pokemon_res: Response = requests.get(pokemon_url)
        pokemon: Dict[str, Any] = pokemon_res.json()
        return pokemon

    @staticmethod
    def _store_pokemon(pokemon: Dict[str, Any]) -> Pokemon:
        pokemon_model_data = _.pick(pokemon, *(
            'id',
            'name',
            'height',
            'weight',
        ))
        pokemon_model: Pokemon
        pokemon_model, pokemon_created = Pokemon.objects.update_or_create(
            defaults=_.omit(pokemon_model_data, Pokemon._meta.pk.name),
            **pokemon_model_data)
        return pokemon_model

    @staticmethod
    def _store_base_stats(stats: List[Dict[str, Any]], pokemon_model):
        def flatten_reducer(_prev_key, next_key):
            return next_key

        stats = _.map_(stats, lambda stat: flatten(stat, reducer=flatten_reducer))
        base_stats = [BaseStat(pokemon=pokemon_model, **stat) for stat in stats]
        base_stats_existing: QuerySet[List[BaseStat]] = \
            BaseStat.objects.filter(pokemon=pokemon_model)

        if base_stats_existing.count() <= 0:
            BaseStat.objects.bulk_create(base_stats)
        else:
            base_stat_field_names = [
                field.name for field in BaseStat._meta.get_fields()
            ]
            fields_to_update = _.filter_(
                base_stat_field_names,
                lambda field_name: field_name != BaseStat._meta.pk.name)
            BaseStat.objects.bulk_update(base_stats_existing, fields_to_update)

    def _store_evolutions(self,
                          pokemon_model: Pokemon,
                          evol_chain: Dict[str, Any],
                          first_in_chain=True,
                          previous_evolution: Evolution = None):
        next_evolutions: List[Dict[str, Any]] = evol_chain['evolves_to']
        evolution_type = 'Pre-evolution' if first_in_chain else 'Evolution'
        evolution_data = {
            'pokemon': pokemon_model,
            'type': evolution_type
        }
        evolution: Evolution
        evolution, created = Evolution.objects.update_or_create(
            defaults=evolution_data, **evolution_data)

        if previous_evolution is not None:
            previous_evolution.evolves_to = evolution
            previous_evolution.save()

        for next_evolution in next_evolutions:
            species = self._fetch_species(next_evolution)
            pokemon = self._fetch_pokemon(species)
            next_pokemon = self._store_pokemon(pokemon)
            self._store_base_stats(pokemon['stats'], next_pokemon)
            self._store_evolutions(next_pokemon,
                                   next_evolution,
                                   first_in_chain=False,
                                   previous_evolution=evolution)
