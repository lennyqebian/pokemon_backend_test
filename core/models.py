from django.db import models
from model_utils.models import TimeStampedModel


class Pokemon(TimeStampedModel):
    class Meta:
        verbose_name = 'Pokémon'
        verbose_name_plural = 'Pokémon'

    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=140)
    height = models.FloatField()
    weight = models.FloatField()


class BaseStat(TimeStampedModel):
    class Meta:
        verbose_name = 'Base stat'
        verbose_name_plural = 'Base stats'

    name = models.CharField(max_length=70)
    url = models.URLField()
    base_stat = models.FloatField()
    effort = models.FloatField()
    pokemon = models.ForeignKey(Pokemon,
                                on_delete=models.CASCADE,
                                related_name='base_stats')


class Evolution(TimeStampedModel):
    class Meta:
        verbose_name = 'Evolution'
        verbose_name_plural = 'Evolutions'

    EVOLUTION_TYPES = (
        ('Pre-evolution', 'Pre-evolution'),
        ('Evolution', 'Evolution'),
    )

    pokemon = models.ForeignKey(Pokemon,
                                on_delete=models.CASCADE,
                                related_name='evolutions')
    evolves_to = models.ForeignKey('self', null=True, on_delete=models.CASCADE)
    type = models.CharField(max_length=20, choices=EVOLUTION_TYPES)
