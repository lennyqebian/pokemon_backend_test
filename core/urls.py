from django.urls import path, re_path

from . import views


app_name = 'persona_app'

urlpatterns = [
  path(
    'pokemon/search/<name>',
    views.PokemonSearchApiView.as_view(),
  ),
]
