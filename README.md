# MO Tecnologies Back-end test

## Instalación y configuración

### Requisitos del sistema

- Python v3.8 o superior

### 1. Pipenv

Asegúrese de instalar la herramienta de gestión de paquetes y entornos virtuales **Pipenv** según la documentación oficial disponible en https://pipenv.pypa.io/en/latest/#install-pipenv-today :

```bash
pip install pipenv
```

### 2. Dependencias de este proyecto

Ejecute el siguiente comando:

```bash
pipenv install
```

Para mayor información visite https://pipenv.pypa.io/en/latest/cli/#pipenv-install .

Si el entorno virtual no se inicia automáticaente, ejecute el comando:

```bash
pipenv shell
```

### 3. Migraciones de base de datos

Para aplicar las migraciones de este proyecto, ejecute el siguiente comando desde el entorno virtual activado en el paso anterior:

```bash
python manage.py migrate
```

## Ejecución

### Servidor local

Para iniciar el servidor local, ejecute el siguiente comando desde el entorno virtual del proyecto:

```bash
python manage.py runserver
```

## Comando Django de almacenamiento de datos

```
$ python manage.py save_evolution_chain --help
usage: manage.py save_evolution_chain [-h] [--version] [-v {0,1,2,3}] [--settings SETTINGS] [--pythonpath PYTHONPATH] [--traceback]
                                      [--no-color] [--force-color] [--skip-checks]
                                      evolution_chain_id

Fetches and stores a Pokémon evolution chain from PokéApi (https://pokeapi.co/docs/v2)

positional arguments:
  evolution_chain_id    ID representing the Evolution Chain

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -v {0,1,2,3}, --verbosity {0,1,2,3}
                        Verbosity level; 0=minimal output, 1=normal output, 2=verbose output, 3=very verbose output
  --settings SETTINGS   The Python path to a settings module, e.g. "myproject.settings.main". If this isn't provided, the
                        DJANGO_SETTINGS_MODULE environment variable will be used.
  --pythonpath PYTHONPATH
                        A directory to add to the Python path, e.g. "/home/djangoprojects/myproject".
  --traceback           Raise on CommandError exceptions
  --no-color            Don't colorize the command output.
  --force-color         Force colorization of the command output.
  --skip-checks         Skip system checks.
```

## API REST

### GET `{{url_base}}/api/pokemon/search/:name`

#### Parámetros

- **`name`**: Término de búsqueda por el nombre del Pokémon.

#### Ejemplo de Petición

```http
GET /api/pokemon/search/weedle HTTP/1.1
User-Agent: PostmanRuntime/7.26.8
Accept: */*
Cache-Control: no-cache
Postman-Token: 404a28dc-2e6c-4c01-829e-7fef34be0c98
Host: 127.0.0.1:8000
Accept-Encoding: gzip, deflate, br
Connection: keep-alive
```

#### Ejemplo de Respuesta Exitosa

```http
HTTP/1.1 200 OK
Date: Mon, 18 Jan 2021 20:43:48 GMT
Server: WSGIServer/0.2 CPython/3.8.5
Content-Type: application/json
Vary: Accept, Cookie
Allow: GET, HEAD, OPTIONS
X-Frame-Options: DENY
Content-Length: 999
X-Content-Type-Options: nosniff
Referrer-Policy: same-origin
```
```json
[
    {
        "id": 13,
        "name": "weedle",
        "height": 3.0,
        "weight": 32.0,
        "base_stats": [
            {
                "id": 1,
                "name": "hp",
                "url": "https://pokeapi.co/api/v2/stat/1/",
                "base_stat": 40.0,
                "effort": 0.0,
                "pokemon": 13
            },
            {
                "id": 2,
                "name": "attack",
                "url": "https://pokeapi.co/api/v2/stat/2/",
                "base_stat": 35.0,
                "effort": 0.0,
                "pokemon": 13
            },
            {
                "id": 3,
                "name": "defense",
                "url": "https://pokeapi.co/api/v2/stat/3/",
                "base_stat": 30.0,
                "effort": 0.0,
                "pokemon": 13
            },
            {
                "id": 4,
                "name": "special-attack",
                "url": "https://pokeapi.co/api/v2/stat/4/",
                "base_stat": 20.0,
                "effort": 0.0,
                "pokemon": 13
            },
            {
                "id": 5,
                "name": "special-defense",
                "url": "https://pokeapi.co/api/v2/stat/5/",
                "base_stat": 20.0,
                "effort": 0.0,
                "pokemon": 13
            },
            {
                "id": 6,
                "name": "speed",
                "url": "https://pokeapi.co/api/v2/stat/6/",
                "base_stat": 50.0,
                "effort": 1.0,
                "pokemon": 13
            }
        ],
        "evolutions": [
            {
                "id": 1,
                "evolves_to": {
                    "id": 2,
                    "evolves_to": {
                        "id": 3,
                        "evolves_to": null,
                        "pokemon": {
                            "id": 15,
                            "name": "beedrill"
                        },
                        "type": "Evolution"
                    },
                    "pokemon": {
                        "id": 14,
                        "name": "kakuna"
                    },
                    "type": "Evolution"
                },
                "pokemon": {
                    "id": 13,
                    "name": "weedle"
                },
                "type": "Pre-evolution"
            }
        ]
    }
]
```
